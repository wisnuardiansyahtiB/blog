<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sign Up</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
    </head>
    <body>
        <h1><b>Buat Akun Baru!</b></h1>
        <h3><b>Sign Up Form</b></h3>
        <form action="/welcome" method="post">
        @csrf
            <h5>First name : </h5>
            <input type="text" name="first_name">
            <h5>Last name : </h5>
            <input type="text" name="last_name">
            <h5>Gender</h5>
            <input type="radio" ><label for="">Male</label><br>
            <input type="radio" ><label for="">Female</label><br>
            <input type="radio" ><label for="">Other</label><br>

            <h5>Nationality</h5>
            <form action="">
                <select name="" id="">
                    <option value="">Indonesia</option>
                    <option value="">Amerika</option>
                    <option value="">Malaysia</option>
                </select>
            </form>
            <h5>Language Spoken</h5>
            <input type="checkbox"><label for="">Bahasa Indonesia</label><br>
            <input type="checkbox"><label for="">English</label><br>
            <input type="checkbox"><label for="">Other</label>

            <h5>Bio</h5>
            <textarea name="" id="" cols="30" rows="10"></textarea><br>
            <input type="submit" value="Sign Up">
        </form>
        
    </body>
</html>